#include <GL/glew.h>
#include "Core/BezierSurface3.h"
#include "Core/Matrices.h"
#include "Bezier/BicubicBezierPatches.h"

using namespace std;
using namespace cagd;


GLboolean BezierSurface3::InsertNewPatch()
{
    //GLuint l = _attribute_vector.size();
    cout <<"a";
    Attributes attr;
    cout <<"b";

    attr.patch->SetData(0, 0, -2.0, -2.0, 0.0);
    cout <<"c";
    attr.patch->SetData(0, 1, -2.0, -1.0, 0.0);
    attr.patch->SetData(0, 2, -2.0, 1.0, 0.0);
    attr.patch->SetData(0, 3, -2.0, 2.0, 0.0);

    attr.patch->SetData(1, 0, -1.0, -2.0, 0.0);
    attr.patch->SetData(1, 1, -1.0, -1.0, 2.0);
    attr.patch->SetData(1, 2, -1.0, 1.0, 2.0);
    attr.patch->SetData(1, 3, -1.0, 2.0, 0.0);

    attr.patch->SetData(2, 0, 1.0, -2.0, 0.0);
    attr.patch->SetData(2, 1, 1.0, -1.0, 2.0);
    attr.patch->SetData(2, 2, 1.0, 1.0, 2.0);
    attr.patch->SetData(2, 3, 1.0, 2.0, 0.0);

    attr.patch->SetData(3, 0, 2.0, -2.0, 0.0);
    attr.patch->SetData(3, 1, 2.0, -1.0, 0.0);
    attr.patch->SetData(3, 2, 2.0, 1.0, 0.0);
    attr.patch->SetData(3, 3, 2.0, 2.0, 0.0);


    attr.mesh_before = attr.patch->GenerateImage(30,30,GL_STATIC_DRAW);

    if( attr.mesh_before )
        attr.mesh_before->UpdateVertexBufferObjects();

    //define an interpolation problem
    //1: creat a knot vector in u-direction
    RowMatrix<GLdouble> u_knot_vector(4);
    u_knot_vector(0) = 0.0;
    u_knot_vector(1) = 1.0 / 3.0;
    u_knot_vector(2) = 2.0 / 3.0;
    u_knot_vector(3) = 1.0;

    //2: create a knot vector in v-direction
    ColumnMatrix<GLdouble> v_knot_vector(4);
    v_knot_vector(0) = 0.0;
    v_knot_vector(1) = 1.0 / 3.0;
    v_knot_vector(2) = 2.0 / 3.0;
    v_knot_vector(3) = 1.0;

    //3: define a matrix of data points, eg. set them to the original control points
    Matrix<DCoordinate3> data_points_to_interpolate(4,4);
    for(GLuint row = 0; row < 4; ++row)
    {
        for(GLuint column = 0; column < 4; ++column)
        {
            attr.patch->GetData(row, column, data_points_to_interpolate(row,column));
        }
    }

    //4:solve the interpolation problem and generate the mesh of the interpolating patch

    if( attr.patch->UpdateDataForInterpolation(u_knot_vector,v_knot_vector, data_points_to_interpolate) )
    {
        attr.mesh_after = attr.patch->GenerateImage(30, 30, GL_STATIC_DRAW);
        if(attr.mesh_after)
            attr.mesh_after->UpdateVertexBufferObjects();
    }

    _attribute_vector.push_back(attr);

    return GL_TRUE;
}

GLvoid BezierSurface3::Render() const
{
    for (GLuint i = 0; i < _attribute_vector.size(); ++i)
    {
        if(_attribute_vector[i].patch)
        {
            if(_attribute_vector[i].mesh_before)
            {
                MatFBRuby.Apply();
                _attribute_vector[i].mesh_before->Render();
            }

            if(_attribute_vector[i].mesh_after)
            {
                glEnable(GL_BLEND);
                glDepthMask(GL_FALSE);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                    MatFBTurquoise.Apply();
                   _attribute_vector[i].mesh_after->Render();
                glDepthMask(GL_TRUE);
                glDisable(GL_BLEND);
            }
        }
    }
}

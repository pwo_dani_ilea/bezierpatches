#pragma once

#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <Bezier/BicubicBezierPatches.h>
#include <Core/TriangulatedMeshes3.h>
#include <Core/Materials.h>
#include <Core/ShaderPrograms.h>
#include <Core/Matrices.h>

using namespace std;
namespace cagd
{
    class BezierSurface3
    {
        public:
            class Attributes
            {
                public:
                    BicubicBezierPatch  *patch;
                    TriangulatedMesh3   *mesh_before, *mesh_after;
                    Material            *material;
                    ShaderProgram       *shader;
                    vector<BicubicBezierPatch> _neighbour;
            };

        protected:
            vector<Attributes> _attribute_vector;
        public:
        GLboolean InsertNewPatch();
        GLvoid Render() const;
    };


}

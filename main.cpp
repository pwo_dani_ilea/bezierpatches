#include <QApplication>
#include "GUI/MainWindow.h"
#include "Core/DCoordinates3.h"
#include "Core/Matrices.h"

using namespace cagd;
using namespace std;

int main(int argc, char** argv)
{
    // creating an application object and setting its style
    QApplication app(argc, argv);
    app.setStyle("Cleanlooks");

    // creating a main window object
    MainWindow mwnd;
    mwnd.showMaximized();

   // fstream log("log.txt", ios_base::out);

    DCoordinate3 a = DCoordinate3(5.0, 6.0, 7.0);
    DCoordinate3 b = DCoordinate3(1.0, 2.0, 3.0);

    //log << a+b <<endl;
    //log.close();
    Matrix <int> m(3,3);
    m(1,1) = 1.0;
    m(1,2) = 1.0;
    m(1,3) = 1.0;
    int d,e ;
    d = m(1,2);
    e = m.GetRowCount();
    m.ResizeRows(5);
    e = m.GetRowCount();
    RowMatrix <int> r(5);
    m.SetRow(2,r);
    cout << m;
    cout << e;
//    M(0,0) = 1.0;
//    M(1,2) = -1.0;
    //DCoordinate3 c;
   // c = a*4.0;
    //a ^= b;

//    cin >> c;
//    cout << c;

    // running the application
    return app.exec();
}

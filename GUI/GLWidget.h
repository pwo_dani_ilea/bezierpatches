#pragma once

// GLU was removed from Qt starting from version 4.8

#include <GL/glew.h>
#include <QGLWidget>
#include <QGLFormat>
#include "../Core/GenericCurves3.h"
#include "../Parametric/ParametricCurves3.h"
#include "../Parametric/ParametricSurfaces3.h"
#include <QTimer>
#include "Bezier/BicubicBezierPatches.h"
#include "Core/BezierSurface3.h"


class GLWidget: public QGLWidget
{
    Q_OBJECT

private:

    QTimer  *_timer;
    GLfloat _angle;

    // variables defining the projection matrix
    float       _aspect;            // aspect ratio of the rendering window
    float       _fovy;              // field of view in direction y
    float       _z_near, _z_far;    // distance of near and far clipping planes

    // variables defining the model-view matrix
    float       _eye[3], _center[3], _up[3];

    // variables needed by transformations
    int         _angle_x, _angle_y, _angle_z;
    double      _zoom;
    double      _trans_x, _trans_y, _trans_z;

    //toggle checkboxes
    bool _curves_visible;
    bool _surfaces_visible;
    bool _mouse_visible;
    bool _project_visible;


    cagd::GenericCurve3 *_icc; //image of the cyclic curve
    GLuint  _mod; //maximal order of derivatives
    GLuint _div;  //subderivetives points

//    cagd::ParametricCurve3* _pc;
//    cagd::GenericCurve3* _image_of_pc;

    GLuint                               _curve_index;
    std::vector<cagd::ParametricCurve3*> _pc;
    std::vector<cagd::GenericCurve3*>    _ipc;

    GLuint                                  _surface_index;
    std::vector<cagd::ParametricSurface3*>  _ps;
    std::vector<cagd::TriangulatedMesh3*>   _ips;


    cagd::TriangulatedMesh3 _model;

    //bicubic Bezier patch
    cagd::BicubicBezierPatch _patch;
    cagd::BezierSurface3 _bezier_surface;
    //std::vector<cagd::BezierSurface3> _bezier_surface;

    //triangulated meshes
    cagd::TriangulatedMesh3 *_before_interpolation, *_after_interpolation;

public:
    // special and default constructor
    // the format specifies the properties of the rendering window
    GLWidget(QWidget* parent = 0, const QGLFormat& format = QGL::Rgba | QGL::DepthBuffer | QGL::DoubleBuffer);

    // redeclared virtual functions
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    virtual ~GLWidget();
private slots:
    void _animate();
public slots:
    // public event handling methods/slots
    void set_angle_x(int value);
    void set_angle_y(int value);
    void set_angle_z(int value);

    void set_zoom_factor(double value);

    void set_trans_x(double value);
    void set_trans_y(double value);
    void set_trans_z(double value);

    void set_curve_index(int value);
    void set_surface_index(int value);

    void toggle_curves(bool value);
    void toggle_surfaces(bool value);
    void toggle_mouse(int value);
    void toggle_project(bool value);
};

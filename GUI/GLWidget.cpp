#include "GLWidget.h"
#include "../Core/Constants.h"
#include "../Core/DCoordinates3.h"
#include "Test/TestFunctions.h"
#include "../Core/Matrices.h"
#include "../Core/Exceptions.h"
#include "../Core/Materials.h"
#include "Core/Lights.h"
#include <QTimer>
#include "Core/BezierSurface3.h"
#include "Bezier/BicubicBezierPatches.h"

#include <iostream>

using namespace std;
using namespace cagd;
//--------------------------------
// special and default constructor
//--------------------------------
GLWidget::GLWidget(QWidget *parent, const QGLFormat &format): QGLWidget(format, parent)
{
    _timer = new QTimer ( this );
    _timer->setInterval( 0 );

    connect(_timer, SIGNAL(timeout()),this, SLOT(_animate()));
    //_timer->start(1000);
}

GLWidget::~GLWidget()
{
    for (GLuint i = 0; i < _pc.size(); ++i)
    {
        if (_pc[i])
        {
            delete _pc[i];
        }

        if (_ipc[i])
        {
            delete _ipc[i];
        }
    }

    for (GLuint i = 0; i < _ps.size(); ++i)
    {
        if (_ps[i])
        {
            delete _ps[i];
        }

        if (_ips[i])
        {
            delete _ips[i];
        }
    }

    if(_before_interpolation)
        delete _before_interpolation, _before_interpolation = 0;

    if(_after_interpolation)
        delete _after_interpolation, _after_interpolation = 0;
}

//--------------------------------------------------------------------------------------
// this virtual function is called once before the first call to paintGL() or resizeGL()
//--------------------------------------------------------------------------------------
void GLWidget::initializeGL()
{
    // creating a perspective projection matrix
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    _aspect = (float)width() / (float)height();
    _z_near = 1.0;
    _z_far = 1000.0;
    _fovy = 45.0;

    gluPerspective(_fovy, _aspect, _z_near, _z_far);

    // setting the model view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    _eye[0] = _eye[1] = 0.0, _eye[2] = 6.0;
    _center[0] = _center[1] = _center[2] = 0.0;
    _up[0] = _up[2] = 0.0, _up[1] = 1.0;

    gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

    // enabling depth test
    glEnable(GL_DEPTH_TEST);

    // setting the color of background
    glClearColor(0.0, 0.0, 0.0, 1.0);

    // initial values of transformation parameters
    _zoom = 1.0;
    _angle_x = _angle_y = _angle_z = 0.0;
    _trans_x = _trans_y = _trans_z = 0.0;

    // initializing the OpenGL Extension Wrangler library
    glewInit();

    // create and store your geometry in display lists or vertex buffer objects
    // ...

    glEnable(GL_POINT_SMOOTH);
       glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
       glEnable(GL_LINE_SMOOTH);
       glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
       glEnable(GL_POLYGON_SMOOTH);
       glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST) ;

       glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST) ;

       glEnable(GL_DEPTH_TEST);

       glewInit();

       GLuint curve_count = 5;
       GLuint surface_count = 5;

       _pc.resize(curve_count);
       _ipc.resize(curve_count);

       _ps.resize(surface_count);
       _ips.resize(surface_count);

       RowMatrix<ParametricCurve3::Derivative> derivative(3);
       TriangularMatrix<ParametricSurface3::PartialDerivative> partial_derivative(2);

       GLuint div_point_count = 200;
       GLuint u_div_point_count = 200;
       GLuint v_div_point_count = 200;
       GLenum usage_flag = GL_STATIC_DRAW;

       try
       {
           //spiral on cone
           derivative(0) = spiral_on_cone::d0;
           derivative(1) = spiral_on_cone::d1;
           derivative(2) = spiral_on_cone::d2;

           _pc[0] = 0;
           _pc[0] = new ParametricCurve3(derivative, spiral_on_cone::u_min, spiral_on_cone::u_max);

           if (!_pc[0])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //trefoil_knot
           derivative(0) = trefoil_knot::d0;
           derivative(1) = trefoil_knot::d1;
           derivative(2) = trefoil_knot::d2;

           _pc[1] = 0;
           _pc[1] = new ParametricCurve3(derivative, trefoil_knot::u_min, trefoil_knot::u_max);

           if (!_pc[1])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }


           //epicycloid
           derivative(0) = epicycloid::d0;
           derivative(1) = epicycloid::d1;
           derivative(2) = epicycloid::d2;

           _pc[2] = 0;
           _pc[2] = new ParametricCurve3(derivative, epicycloid::u_min, epicycloid::u_max);

           if (!_pc[2])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //hypocycloid
           derivative(0) = hypocycloid::d0;
           derivative(1) = hypocycloid::d1;
           derivative(2) = hypocycloid::d2;

           _pc[3] = 0;
           _pc[3] = new ParametricCurve3(derivative, hypocycloid::u_min, hypocycloid::u_max);

           if (!_pc[3])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //viviani
           derivative(0) = viviani::d0;
           derivative(1) = viviani::d1;
           derivative(2) = viviani::d2;

           _pc[4] = 0;
           _pc[4] = new ParametricCurve3(derivative, viviani::u_min, viviani::u_max);

           if (!_pc[4])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //torus

           partial_derivative(0, 0) = torus::d00;
           partial_derivative(1, 0) = torus::d10;
           partial_derivative(1, 1) = torus::d01;

           _ps[0] = 0;
           _ps[0] = new ParametricSurface3(partial_derivative, torus::u_min, torus::u_max, torus::v_min, torus::v_max);

           if (!_ps[0])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //alfred
           partial_derivative(0, 0) = alfred::d00;
           partial_derivative(1, 0) = alfred::d10;
           partial_derivative(1, 1) = alfred::d01;

           _ps[1] = 0;
           _ps[1] = new ParametricSurface3(partial_derivative, alfred::u_min, alfred::u_max, alfred::v_min, alfred::v_max);

           if (!_ps[1])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //helicoid
           partial_derivative(0, 0) = helicoid::d00;
           partial_derivative(1, 0) = helicoid::d10;
           partial_derivative(1, 1) = helicoid::d01;

           _ps[2] = 0;
           _ps[2] = new ParametricSurface3(partial_derivative, helicoid::u_min, helicoid::u_max, helicoid::v_min, helicoid::v_max);

           if (!_ps[2])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //hybrid
           partial_derivative(0, 0) = hybrid::d00;
           partial_derivative(1, 0) = hybrid::d10;
           partial_derivative(1, 1) = hybrid::d01;

           _ps[3] = 0;
           _ps[3] = new ParametricSurface3(partial_derivative, hybrid::u_min, hybrid::u_max, hybrid::v_min, hybrid::v_max);

           if (!_ps[3])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           //torus_circle
           partial_derivative(0, 0) = torus_circle::d00;
           partial_derivative(1, 0) = torus_circle::d10;
           partial_derivative(1, 1) = torus_circle::d01;

           _ps[4] = 0;
           _ps[4] = new ParametricSurface3(partial_derivative, torus_circle::u_min, torus_circle::u_max, torus_circle::v_min, torus_circle::v_max);

           if (!_ps[4])
           {
               throw Exception("Could not allocate memory for the spiral on cone!");
           }

           // ...

           //loadin the model
           glewInit();
           if(_model.LoadFromOFF("Models/mouse.off", true))
           {
               if(_model.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW))
               {
                   _angle = 0.0;
                   _timer->start();
               }
           }

           for (GLuint i = 0; i < _pc.size(); ++i)
           {
               _ipc[i] = 0;
               _ipc[i] = _pc[i]->GenerateImage(div_point_count, usage_flag);

               if (!_ipc[i])
               {
                   throw Exception("Could not create the image!");
               }

               if (!_ipc[i]->UpdateVertexBufferObjects(1, usage_flag))
               {
                   cout << "Could not create the vertex buffer object of the parametric curve!" << endl;
               }

               //cout << (*_ipc[i]) << endl;
           }

           //cout << "BF\n";

           for (GLuint i = 0; i < _ps.size(); ++i)
           {
               _ips[i] = 0;
               //cout << "\tBGI i=" << i << endl;
               _ips[i] = _ps[i]->GenerateImage(u_div_point_count, v_div_point_count, usage_flag);
               //cout << "\tAGI i=" << i << endl;

               if (!_ips[i])
               {
                   throw Exception("Could not create the image!");
               }

               if (!_ips[i]->UpdateVertexBufferObjects())
               {
                   cout << "Could not create the vertex buffer object of the parametric curve!" << endl;
               }

               //cout << (*_ipc[i]) << endl;
           }

           _curve_index = 0;
           _surface_index = 0;

           // initialize Bezier patch
               glewInit();
               _bezier_surface.InsertNewPatch();
//               _patch.SetData(0, 0, -2.0, -2.0, 0.0);
//               _patch.SetData(0, 1, -2.0, -1.0, 0.0);
//               _patch.SetData(0, 2, -2.0, 1.0, 0.0);
//               _patch.SetData(0, 3, -2.0, 2.0, 0.0);

//               _patch.SetData(1, 0, -1.0, -2.0, 0.0);
//               _patch.SetData(1, 1, -1.0, -1.0, 2.0);
//               _patch.SetData(1, 2, -1.0, 1.0, 2.0);
//               _patch.SetData(1, 3, -1.0, 2.0, 0.0);

//               _patch.SetData(2, 0, 1.0, -2.0, 0.0);
//               _patch.SetData(2, 1, 1.0, -1.0, 2.0);
//               _patch.SetData(2, 2, 1.0, 1.0, 2.0);
//               _patch.SetData(2, 3, 1.0, 2.0, 0.0);

//               _patch.SetData(3, 0, 2.0, -2.0, 0.0);
//               _patch.SetData(3, 1, 2.0, -1.0, 0.0);
//               _patch.SetData(3, 2, 2.0, 1.0, 0.0);
//               _patch.SetData(3, 3, 2.0, 2.0, 0.0);

//               //generate the mesh of the surface path
//               _before_interpolation = _patch.GenerateImage(30, 30, GL_STATIC_DRAW);

//               if( _before_interpolation )
//                   _before_interpolation->UpdateVertexBufferObjects();

//               //define an interpolation problem
//               //1: creat a knot vector in u-direction
//               RowMatrix<GLdouble> u_knot_vector(4);
//               u_knot_vector(0) = 0.0;
//               u_knot_vector(1) = 1.0 / 3.0;
//               u_knot_vector(2) = 2.0 / 3.0;
//               u_knot_vector(3) = 1.0;

//               //2: create a knot vector in v-direction
//               ColumnMatrix<GLdouble> v_knot_vector(4);
//               v_knot_vector(0) = 0.0;
//               v_knot_vector(1) = 1.0 / 3.0;
//               v_knot_vector(2) = 2.0 / 3.0;
//               v_knot_vector(3) = 1.0;

//               //3: define a matrix of data points, eg. set them to the original control points
//               Matrix<DCoordinate3> data_points_to_interpolate(4,4);
//               for(GLuint row = 0; row < 4; ++row)
//               {
//                   for(GLuint column = 0; column < 4; ++column)
//                   {
//                       _patch.GetData(row, column, data_points_to_interpolate(row,column));
//                   }
//               }

//               //4:solve the interpolation problem and generate the mesh of the interpolating patch

//               if( _patch.UpdateDataForInterpolation(u_knot_vector,v_knot_vector, data_points_to_interpolate) )
//               {
//                   _after_interpolation = _patch.GenerateImage(30, 30, GL_STATIC_DRAW);
//                   if(_after_interpolation)
//                       _after_interpolation->UpdateVertexBufferObjects();
//               }
            //}


       }
       catch (Exception &e)
       {
           cout << e << endl;
       }

       glEnable(GL_LIGHTING);
       glEnable(GL_NORMALIZE);
       glEnable(GL_LIGHT0);
}

//-----------------------
// the rendering function
//-----------------------
void GLWidget::paintGL()
{
    // clears the color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // stores/duplicates the original model view matrix
    glPushMatrix();

        // applying transformations
        glRotatef(_angle_x, 1.0, 0.0, 0.0);
        glRotatef(_angle_y, 0.0, 1.0, 0.0);
        glRotatef(_angle_z, 0.0, 0.0, 1.0);

        glTranslated(_trans_x, _trans_y, _trans_z);

        glScaled(_zoom, _zoom, _zoom);

//        // render your geometry
//        glBegin(GL_TRIANGLES);
//            glColor3f(1.0, 0.0, 0.0);
//            glVertex3f(1.0, 0.0, 0.0);

//            glColor3f(0.0, 1.0, 0.0);
//            glVertex3f(0.0, 1.0, 0.0);

//            glColor3f(0.0, 0.0, 1.0);
//            glVertex3f(0.0, 0.0, 1.0);
//        glEnd();

    // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
    // i.e., the original model view matrix is restored
        if(_curves_visible)
        {
            glDisable(GL_LIGHTING);
            if ( _ipc[_curve_index] )
             {
                 glColor3f(1.0, 0.0, 0.0);
                 _ipc[_curve_index]->RenderDerivatives(0, GL_LINE_STRIP);

                 glPointSize(5.0);

                     glColor3f(0.0, 0.5, 0.0);
                     _ipc[_curve_index]->RenderDerivatives(1, GL_LINES);
                     _ipc[_curve_index]->RenderDerivatives(1, GL_POINTS);

                     glColor3f(0.1, 0.5, 0.9);
                     _ipc[_curve_index]->RenderDerivatives(2, GL_LINES);
                     _ipc[_curve_index]->RenderDerivatives(2, GL_POINTS);

                 glPointSize(1.0);

             }
        }

        if(_surfaces_visible)
        {
            glEnable(GL_LIGHTING);
            if ( _ips[_surface_index] )
             {
                 MatFBBrass.Apply();
                 _ips[_surface_index]->Render();
             }
        }

        if(_mouse_visible)
        {

            glEnable(GL_LIGHTING);
            MatFBPearl.Apply( ) ;
            _model.Render();
        }

        if (_project_visible )
        {
//            if(_before_interpolation)
//            {
//                MatFBRuby.Apply();
//                _before_interpolation->Render();
//            }

//            if(_after_interpolation)
//            {
//                glEnable(GL_BLEND);
//                glDepthMask(GL_FALSE);
//                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
//                    MatFBTurquoise.Apply();
//                    _after_interpolation->Render();
//                glDepthMask(GL_TRUE);
//                glDisable(GL_BLEND);
//            }
           // _bezier_surface.Render();
        }



    glPopMatrix();
}

//----------------------------------------------------------------------------
// when the main window is resized one needs to redefine the projection matrix
//----------------------------------------------------------------------------
void GLWidget::resizeGL(int w, int h)
{
    // setting the new size of the rendering context
    glViewport(0, 0, w, h);

    // redefining the projection matrix
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    _aspect = (float)w / (float)h;

    gluPerspective(_fovy, _aspect, _z_near, _z_far);

    // switching back to the model view matrix
    glMatrixMode(GL_MODELVIEW);

    updateGL();
}

//-----------------------------------
// implementation of the public slots
//-----------------------------------

void GLWidget::set_angle_x(int value)
{
    if (_angle_x != value)
    {
        _angle_x = value;
        updateGL();
    }
}

void GLWidget::set_angle_y(int value)
{
    if (_angle_y != value)
    {
        _angle_y = value;
        updateGL();
    }
}

void GLWidget::set_angle_z(int value)
{
    if (_angle_z != value)
    {
        _angle_z = value;
        updateGL();
    }
}

void GLWidget::set_zoom_factor(double value)
{
    if (_zoom != value)
    {
        _zoom = value;
        updateGL();
    }
}

void GLWidget::set_trans_x(double value)
{
    if (_trans_x != value)
    {
        _trans_x = value;
        updateGL();
    }
}

void GLWidget::set_trans_y(double value)
{
    if (_trans_y != value)
    {
        _trans_y = value;
        updateGL();
    }
}

void GLWidget::set_trans_z(double value)
{
    if (_trans_z != value)
    {
        _trans_z = value;
        updateGL();
    }
}

void GLWidget::set_curve_index(int value)
{
    if (_curve_index != value)
    {
        _curve_index = value;

        updateGL();
    }
}

void GLWidget::set_surface_index(int value)
{
    if (_surface_index != value)
    {
        _surface_index = value;

        updateGL();
    }
}

void GLWidget::_animate()
{
    GLfloat *vertex = _model.MapVertexBuffer(GL_READ_WRITE);
    GLfloat *normal = _model.MapNormalBuffer(GL_READ_ONLY);

    _angle += DEG_TO_RADIAN;
    if(_angle >= TWO_PI) _angle -= TWO_PI;

    GLfloat scale = sin(_angle)/3000.0;
    for(GLuint i = 0; i < _model.VertexCount(); ++i)
    {
        for(GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal)
            *vertex += scale * (*normal);
    }

    _model.UnmapVertexBuffer();
    _model.UnmapNormalBuffer();

    updateGL();

}

void GLWidget::toggle_curves(bool value)
{
    _curves_visible = value;
    updateGL();
}

void GLWidget::toggle_surfaces(bool value)
{
    _surfaces_visible = value;
    updateGL();
}

void GLWidget::toggle_mouse(int value)
{
    if(_mouse_visible != value )
    {
        _mouse_visible = value;
        updateGL();
    }
}
void GLWidget::toggle_project(bool value)
{
    _project_visible = value;
    updateGL();
}

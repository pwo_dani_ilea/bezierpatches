SOURCES += \
    main.cpp \
    GUI/MainWindow.cpp \
    GUI/GLWidget.cpp \
    GUI/SideWidget.cpp \
    Core/GenericCurves3.cpp \
    Parametric/ParametricCurves3.cpp \
    Test/TestFunctions.cpp \
    Core/RealSquareMatrices.cpp \
    Core/LinearCombination3.cpp \
    Core/TriangulatedMeshes3.cpp \
    Core/Materials.cpp \
    Core/Lights.cpp \
    Parametric/ParametricSurfaces3.cpp \
    Core/TensorProductSurfaces3.cpp \
    Bezier/BicubicBezierPatches.cpp \
    Core/ShaderPrograms.cpp \
    Core/BezierSurface3.cpp

HEADERS += \
    GUI/MainWindow.h \
    GUI/GLWidget.h \
    GUI/SideWidget.h \
    Core/Matrices.h \
    Core/DCoordinates3.h \
    Core/GenericCurves3.h \
    Core/Constants.h \
    Parametric/ParametricCurves3.h \
    Test/TestFunctions.h \
    Core/RealSquareMatrices.h \
    Core/LinearCombination3.h \
    Core/Exceptions.h \
    Core/TriangulatedMeshes3.h \
    Core/TriangularFaces.h \
    Core/TCoordinates4.h \
    Core/Materials.h \
    Core/Lights.h \
    Core/HCoordinates3.h \
    Core/Colors4.h \
    Parametric/ParametricSurfaces3.h \
    Core/TensorProductSurfaces3.h \
    Bezier/BicubicBezierPatches.h \
    Core/ShaderPrograms.h \
    Core/BezierSurface3.h

FORMS += \
    GUI/MainWindow.ui \
    GUI/SideWidget.ui \

QT += opengl

CONFIG += console

INCLUDEPATH += ./Dependencies/Include
LIBS += ./Dependencies/Lib/GL/glew32.dll ./Dependencies/Lib/GL/glew32.lib

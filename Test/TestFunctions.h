#pragma once

#include "../Core/DCoordinates3.h"

namespace cagd
{
    namespace spiral_on_cone
    {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace trefoil_knot
    {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace epicycloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble r,k;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace hypocycloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble r,k;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace viviani
    {
        extern GLdouble u_min, u_max;
        extern GLdouble a;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace torus
    {
        // possible shape parameters
        extern GLdouble R,r;

        // definition domain
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v); // zeroth order partial derivative, i.e. surface point
        DCoordinate3 d10(GLdouble u, GLdouble v); // first order partial derivative in direction u
        DCoordinate3 d01(GLdouble u, GLdouble v); // first order partial derivative in direction v
    }

    namespace torus_circle
    {
        // possible shape parameters
        extern GLdouble R,r;

        // definition domain
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v); // zeroth order partial derivative, i.e. surface point
        DCoordinate3 d10(GLdouble u, GLdouble v); // first order partial derivative in direction u
        DCoordinate3 d01(GLdouble u, GLdouble v); // first order partial derivative in direction v
    }

    namespace alfred
    {
        // possible shape parameters
        //extern GLdouble R,r;

        // definition domain
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v); // zeroth order partial derivative, i.e. surface point
        DCoordinate3 d10(GLdouble u, GLdouble v); // first order partial derivative in direction u
        DCoordinate3 d01(GLdouble u, GLdouble v); // first order partial derivative in direction v
    }

    namespace helicoid
    {
        // possible shape parameters
        //extern GLdouble R,r;

        // definition domain
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v); // zeroth order partial derivative, i.e. surface point
        DCoordinate3 d10(GLdouble u, GLdouble v); // first order partial derivative in direction u
        DCoordinate3 d01(GLdouble u, GLdouble v); // first order partial derivative in direction v
    }

    namespace hybrid
    {
        // possible shape parameters
        //extern GLdouble R,r;

        // definition domain
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v); // zeroth order partial derivative, i.e. surface point
        DCoordinate3 d10(GLdouble u, GLdouble v); // first order partial derivative in direction u
        DCoordinate3 d01(GLdouble u, GLdouble v); // first order partial derivative in direction v
    }
}
